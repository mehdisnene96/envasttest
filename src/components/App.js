import React from "react";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import { Row } from "react-bootstrap";

import { EventForm } from "./event-form";
import { EventsList } from "./events-list";
import { CommentList } from "./comments-list";
import { getEvents, getEventComments, getEmployees } from "../data/data";

export class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      comments: [],
      events: [],
      employee: [],
      newComment: {},
      isNewComment: false,
      selected: {
        id: 47,
        creationDate: "2008-03-30T05:13:23Z",
        createdBy: "Kirstin",
        involvedEmployee: {
          id: 1868,
          firstname: "Adelle",
          lastname: "Thornburg",
        },
        title: "Accident avec Adelle",
        description: "",
        statusName: "Open",
        Témoins: ["Mureil", "Melina"],
      },
      editComment: null,
    };
    this.onSelect = this.onSelect.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.onDeleteComment = this.onDeleteComment.bind(this);
    this.onEditComment = this.onEditComment.bind(this);
    this.handleCommentChange = this.handleCommentChange.bind(this);
    this.onCancelComment = this.onCancelComment.bind(this);
  }
  async componentDidMount() {
    this.setState({
      comments: getEventComments(this.state.selected.id),
      events: getEvents(),
      employee: getEmployees(),
    });
    
  }
  onDeleteComment(event) {
    const creationDate = event.target.id;
    

    const cloneComments = [...this.state.comments];
    const comments = cloneComments.filter(
      (comment) => comment.creationDate !== creationDate
    );
    
    this.setState({ comments });
  }
  onEditComment(event) {
    const creationDate = event.target.id
      ? event.target.id
      : Date().toLocaleString();

    const newCommentIndex = this.state.comments.findIndex(
      (comment) => comment.creationDate === creationDate
    );
    if (newCommentIndex === -1) {
      this.setState({
        editComment: creationDate,
        newComment: { creationDate, author: "", content: "" },
        isNewComment: true,
      });
    } else {
      this.setState({
        editComment: creationDate,
        newComment: [...this.state.comment][newCommentIndex],
      });
    }
  }
  onSelect(event) {
    let selectedId = event.target.id;
    const comments = getEventComments(parseInt(selectedId));
    const selected = [...this.state.events].find(
      (item) => item.id === parseInt(selectedId)
    );

    this.setState({ selected, comments });
  }
  onCancelComment() {
    this.setState({ newComment: {}, editComment: null ,isNewComment:false});
  }
  handleCommentChange(event) {
    const name = event.target.name;
    let value = event.target.value;

    const cloneNewComment = { ...this.state.newComment };
    cloneNewComment[name] = value;

    this.setState({
      newComment: cloneNewComment,
    });
    
  }
  handleInputChange(event) {
    const name = event.target.name;
    let value = event.target.value;
    if (name === "involvedEmployee") {
      value = this.state.employee.find((emp) => emp.id === parseInt(value));
    }
    const cloneSelected = { ...this.state.selected };
    cloneSelected[name] = value;

    this.setState({
      selected: cloneSelected,
    });
    
  }

   onSubmit(e) {
    const cloneEvents = [...this.state.events]
    const index = cloneEvents.findIndex(
      (event) => event.id === this.state.selected.id
    );
    cloneEvents[index]=this.state.selected;
    this.setState({events:cloneEvents})
    e.preventDefault();
  }
  onSave(e) {
    
    const cloneComments = [...this.state.comments];

    const index = cloneComments.findIndex(
      (comment) => comment.creationDate === this.state.newComment.creationDate
    );
    if (index === -1) {
      cloneComments.unshift(this.state.newComment);
    } else {
      cloneComments[index] = this.state.newComment;
    }
    this.setState({
      comments: cloneComments,
      newComment: {},
      editComment: null,
      isNewComment:false
    });

    e.preventDefault();
  }

  render() {
    return (
      <Container fluid>
        <Row>
          <Col>
            <br />
            <h3>Mes évènements</h3>
            <EventsList
              events={this.state.events}
              selected={this.state.selected}
              onSelect={this.onSelect}
            />
          </Col>
          <Col>
            <br />
            <EventForm
              event={this.state.selected}
              involvedEmployees={this.state.employee}
              handleChange={this.handleInputChange}
              onSubmit={this.onSubmit}
            />
          </Col>
          <Col>
            <CommentList
              comments={this.state.comments}
              onDelete={this.onDeleteComment}
              editComment={this.state.editComment}
              onEdit={this.onEditComment}
              handleChange={this.handleCommentChange}
              newComment={this.state.newComment}
              onCancel={this.onCancelComment}
              onSave={this.onSave}
              isNewComment={this.state.isNewComment}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}
