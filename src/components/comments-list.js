import React from "react";
import { Card, Button, Form, Container, Row, Col } from "react-bootstrap";

export class CommentList extends React.Component {
  render() {
    return (
      <div>
      { !this.props.editComment &&  <Button variant="primary" onClick={this.props.onEdit}>
          add
        </Button>}
        {(this.props.isNewComment) && <Container>
                <Row>
                  <Col>
                    <Form>
                      <Form.Group>
                        <Form.Label>Auteur</Form.Label>
                        <Form.Control
                          name="author"
                          value={this.props.newComment.author}
                          onChange={this.props.handleChange}
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Content</Form.Label>
                        <Form.Control
                          as="textarea"
                          rows={3}
                          name="content"
                          value={this.props.newComment.content}
                          onChange={this.props.handleChange}
                        />
                      </Form.Group>
                      <Button
                        variant="primary"

                        onClick={this.props.onSave}
                      >
                        Save
                      </Button>

                      <Button variant="danger" onClick={this.props.onCancel}>
                        cancel
                      </Button>
                    </Form>
                  </Col>
                </Row>
              </Container>}
        <br/>
        
        <br/>
        {this.props.comments.map((comment) => {
          if (this.props.editComment === comment.creationDate) {
            return (
              <Container >
                <Row>
                  <Col>
                    <Form>
                      <Form.Group>
                        <Form.Label>Auteur</Form.Label>
                        <Form.Control
                          name="author"
                          value={this.props.newComment.author}
                          onChange={this.props.handleChange}
                        />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Content</Form.Label>
                        <Form.Control
                          as="textarea"
                          rows={3}
                          name="content"
                          value={this.props.newComment.content}
                          onChange={this.props.handleChange}
                        />
                      </Form.Group>
                      <Button
                        variant="primary"
                        id={comment.creationDate}
                        onClick={this.props.onSave}
                      >
                        Save
                      </Button>

                      <Button variant="danger" onClick={this.props.onCancel}>
                        cancel
                      </Button>
                    </Form>
                  </Col>
                </Row>
              </Container>
            );
          }
          return (
            <Card key={comment.creationDate}>
              <Card.Body>
                <Card.Title>{comment.author} </Card.Title>
                <Card.Text>{comment.content}</Card.Text>

                <Button
                  variant="primary"
                  id={comment.creationDate}
                  onClick={this.props.onEdit}
                >
                  edit
                </Button>

                <Button
                  variant="danger"
                  id={comment.creationDate}
                  onClick={this.props.onDelete}
                >
                  supprimer
                </Button>
              </Card.Body>
            </Card>
          );
        })}
      </div>
    );
  }
}
