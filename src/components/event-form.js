import React from "react";
import { Button, Col } from "react-bootstrap";
import Form from "react-bootstrap/Form";

export class EventForm extends React.Component {
  render() {
    return (
      <div>
        <h3>{this.props.event.title}</h3>
        <h6>{`Crée le ${this.props.event.creationDate.substring(
          0,
          10
        )} à ${this.props.event.creationDate.substring(11, 19)}`}</h6>
        <br />

        <Form onSubmit={this.props.onSubmit}>
          <Form.Group controlId="title">
            <Form.Label>Titre</Form.Label>
            <Form.Control
              placeholder="Titre"
              name="title"
              value={this.props.event.title}
              onChange={this.props.handleChange}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Description</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              name="description"
              value={this.props.event.description}
              onChange={this.props.handleChange}
            />
          </Form.Group>
          <Form.Row>
            <Form.Group as={Col} controlId="date">
              <Form.Label>Select Date</Form.Label>
              <Form.Control
                type="date"
                name="dob"
                value={this.props.event.creationDate.substring(0, 10)}
                disabled
              />
            </Form.Group>
            <Form.Group as={Col} controlId="time">
              <Form.Label>Select time</Form.Label>
              <Form.Control
                type="time"
                name="dob"
                value={this.props.event.creationDate.substring(11, 19)}
                disabled
              />
            </Form.Group>
          </Form.Row>

          <Form.Group controlId="formGridState">
            <Form.Label>Nom du statut</Form.Label>
            <Form.Control
              as="select"
              name="statusName"
              value={this.props.event.statusName}
              onChange={this.props.handleChange}
            >
              <option>Open</option>
              <option>InProgress</option>
              <option>Closed</option>
            </Form.Control>
          </Form.Group>
          <Form.Group>
            <Form.Label>Employé impliqué</Form.Label>
            <Form.Control
              as="select"
              name="involvedEmployee"
              defaultValue={`${this.props.event.involvedEmployee.firstname} ${this.props.event.involvedEmployee.lastname}`}
              onChange={this.props.handleChange}
            >
              {this.props.involvedEmployees.map(({id,firstname,lastname}) => {
                return (
                  <option key={id}
                    value={id}
                  >{`${firstname} ${lastname}`}</option>
                );
              })}
            </Form.Control>
          </Form.Group>

          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
      </div>
    );
  }
}
