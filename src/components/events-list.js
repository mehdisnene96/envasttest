import React from "react";
import { ListGroup } from "react-bootstrap";

export class EventsList extends React.Component {
  static defaultProps = {
    events: [],
  };

  render() {
    return (
      <ListGroup as="ul">
        {this.props.events.map((event) => {
            //console.log(event);
          return <ListGroup.Item key={event.id} id={event.id} as="li" active={event.id === this.props.selected.id} onClick={this.props.onSelect}>
            {event.title}
          </ListGroup.Item>;
        })}
        
      </ListGroup>
    );
  }
}
